import 'package:flutter/material.dart';
import 'package:life_cycle/main_app.dart';

void main() {
  runApp(const MainApp());
}
