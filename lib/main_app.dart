import 'package:flutter/material.dart';
import 'package:life_cycle/app_life_cycle.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(body: AppLifecycleDisplay()),
    );
  }
}
